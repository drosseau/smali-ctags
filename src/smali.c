#include "general.h"

#include <ctype.h>
#include <string.h>

#include "entry.h"
#include "options.h"
#include "parse.h"
#include "read.h"

typedef enum {
    K_CLASS,
    K_METHOD,
    K_FIELD,
} smaliKind;

static const char* CLASS_DEF = ".class";
static const size_t CLASS_DEF_LEN = 6;
static const char* METHOD_DEF = ".method";
static const size_t METHOD_DEF_LEN = 7;
static const char* FIELD_DEF = ".field";
static const size_t FIELD_DEF_LEN = 6;
static const char* IMPLEMENTS_PREF = ".implements";
static const size_t IMPLEMENTS_PREF_LEN = 11;

static kindOption SmaliKinds[] = {
    { 1, 'c', "class", "class definition" },
    { 1, 'm', "method", "method definition" },
    { 1, 'f', "field", "field definition" },
};

/**
 * Eat up the whitespace and return the pointer at the first non whitespace
 * character. Could return NULL.
 */
static const unsigned char* chompWhitespace(const unsigned char* line)
{
    while (line != NULL && isspace((int)*line)) {
        ++line;
    }
    return line;
}

typedef struct _parser {
    vString* className;
    vString* vs;
    vString* ovs;
    struct {
        vString** names;
        size_t cap;
        size_t size;
    } interfaces;
    const unsigned char* line;
} parser;

static const char* MODIFIERS[] = {
    "public",
    "private",
    "protected",
    "static",
    "bridge",
    "declared-synchronized",
    "synthetic",
    "varargs",
    "final",
    "abstract",
    "constructor",
    "blacklist",
    "whitelist",
    "greylist",
    "greylist-max-o",
    "greylist-max-p",
    "greylist-max-q",
    "greylist-max-r",
    // Don't remove this NULL!
    NULL
};

/**
 * Chomp visibility modifiers
 */
static const unsigned char* chompModifiers(const unsigned char* line, unsigned char end)
{
    size_t i = 0, j = 0, len = 0;
    const char** modp = NULL;
    int found = 0;
    while ((line = chompWhitespace(line)) != NULL) {
        i = 0;
        while (line != NULL && !(isspace((int)*line)) && *line != end) {
            ++line;
            ++i;
        }
        found = 0;
        for (modp = MODIFIERS; *modp != NULL; ++modp) {
            len = strlen(*modp);
            if (len != i) {
                continue;
            }
            if (strncmp((const char*)(line - i), *modp, i) == 0) {
                found = 1;
                break;
            }
        }
        if (found == 0) {
            return (line - i);
        }
    }
    return NULL;
}

static int SMALI_CTAGS_ESCAPE_VIM = 0;

/**
 * There is a problem when a tag contains a $.
 *
 * We seem to be screwed with filenames that contain that, but I can fix the
 * tags to be forced to line number instead.
 */
static void makeSmaliTagEntry(const vString* const name, kindOption* const kinds, const int kind, const parser* ctx)
{
    // We don't need to do anything special with these
    if (Option.locate == EX_LINENUM || Option.etags || Option.xref) {
        makeSimpleTag(name, kinds, kind);
        return;
    }
    if (kinds[kind].enabled && name != NULL && vStringLength(name) > 0) {
        tagEntryInfo e;
        initTagEntry(&e, vStringValue(name));

        e.kindName = kinds[kind].name;
        e.kind = kinds[kind].letter;

        // Force line number entries when the tag contains a $
        if (e.lineNumberEntry != TRUE && (index(vStringValue(name), '$') != NULL || (ctx != NULL && index(vStringValue(ctx->className), '$') != NULL))) {
            e.lineNumberEntry = TRUE;
        }
        if (SMALI_CTAGS_ESCAPE_VIM && index(e.sourceFileName, '$') != NULL) {
            vString* newFile = vStringNew();
            if (newFile == NULL) {
                printf("Failed to allocate a new vString to hold a file\n");
                exit(1);
            }
            while (e.sourceFileName != NULL && *e.sourceFileName != '\0') {
                if (*e.sourceFileName == '$') {
                    vStringPut(newFile, (int)'\\');
                }
                vStringPut(newFile, (int)*e.sourceFileName);
                ++e.sourceFileName;
            }
            vStringTerminate(newFile);
            e.sourceFileName = vStringValue(newFile);
            makeTagEntry(&e);
            vStringDelete(newFile);
        } else {
            makeTagEntry(&e);
        }
    }
}

static void doParseItem(parser* ctx, unsigned char endChar, smaliKind kind)
{
    if (ctx->line == NULL || ctx->className == NULL)
        return;
    const unsigned char* line = chompModifiers(ctx->line, endChar);
    if (line == NULL)
        return;
    line = chompWhitespace(line);
    if (line == NULL)
        return;
    vStringClear(ctx->ovs);
    while (line != NULL && !isspace((int)*line) && *line != endChar) {
        if (SMALI_CTAGS_ESCAPE_VIM && *line == '[') {
            vStringPut(ctx->ovs, (int)'\\');
        }
        vStringPut(ctx->ovs, (int)*line);
        ++line;
    }
    if (line != NULL) {
        if (SMALI_CTAGS_ESCAPE_VIM && *line == '[') {
            vStringPut(ctx->ovs, (int)'\\');
        }
        vStringPut(ctx->ovs, (int)*line);
    }
    vStringTerminate(ctx->ovs);
    if (kind == K_METHOD) {
        // Interface hack:
        //  Just assume every method belongs to the interface. We'll only ever
        //  search for the tags of the real one anyway. The worst this does is
        //  inflate the size of the tags file. I can live with that.
        if (ctx->interfaces.size > 0 && strncmp("<init>", vStringValue(ctx->ovs), 6) != 0
            && strncmp("<clinit>", vStringValue(ctx->ovs), 8) != 0) {
            for (int i = 0; i < ctx->interfaces.size; ++i) {
                vString* iface = ctx->interfaces.names[i];
                vStringNCopy(ctx->vs, iface, vStringLength(iface));
                vStringNCatS(ctx->vs, ";->", 3);
                vStringNCat(ctx->vs, ctx->ovs, vStringLength(ctx->ovs));
                makeSmaliTagEntry(ctx->vs, SmaliKinds, kind, ctx);
            }
        }
    }
    // Make the tag including the class
    vStringNCopy(ctx->vs, ctx->className, vStringLength(ctx->className));
    vStringNCatS(ctx->vs, ";->", 3);
    vStringNCat(ctx->vs, ctx->ovs, vStringLength(ctx->ovs));
    makeSmaliTagEntry(ctx->vs, SmaliKinds, kind, ctx);
}

/**
 * These following parseX functions expect chompWhitespace to have already
 * been called.
 */

/**
 * parseClass will parse a class from the loaded line and return whether or
 * not it is an interface. It will return -1 on failure.
 */
static int parseClass(parser* ctx)
{
    int ret = 0;
    if (ctx->line == NULL)
        return -1;
    ctx->className = vStringNew();
    if (ctx->className == NULL) {
        printf("Failed to allocate a new vString to hold a class name\n");
        exit(1);
    }
    vString* mod = vStringNew();
    if (mod == NULL) {
        printf("Failed to allocate a new vString to hold a module name\n");
        exit(1);
    }
    // Let's actually check to see if this is an interface. If it is I think
    // we should stop making tags after we get the interface definition tag.
    while (ctx->line != NULL && *ctx->line != 'L') {
        if (!ret) {
            if (isspace((int)*ctx->line)) {
                if (strncmp("interface", vStringValue(mod), vStringLength(mod)) == 0) {
                    ret = 1;
                }
                vStringClear(mod);
            } else {
                vStringPut(mod, (int)*ctx->line);
            }
        }
        ++ctx->line;
    }
    if (ctx->line == NULL) {
        ret = -1;
        goto leave;
    }
    // Now read increment until a ;
    while (ctx->line != NULL && *(ctx->line) != ';') {
        vStringPut(ctx->className, (int)*(ctx->line));
        ++ctx->line;
    }
    // If line is null something has gone wrong
    if (ctx->line == NULL) {
        ret = -1;
        goto leave;
    }
    vStringTerminate(ctx->className);
    makeSmaliTagEntry(ctx->className, SmaliKinds, K_CLASS, NULL);
leave:
    vStringDelete(mod);
    return ret;
}

/**
 * addInterface expects a null terminated vString
 */
static void addInterface(parser* ctx, const vString* interface)
{
    if (ctx->interfaces.names == NULL) {
        ctx->interfaces.cap = 5;
        ctx->interfaces.size = 0;
        ctx->interfaces.names = calloc(ctx->interfaces.cap, sizeof(vString*));
        if (ctx->interfaces.names == NULL) {
            printf("Failed to allocate interface names array\n");
            exit(1);
        }
    } else {
        while (ctx->interfaces.size >= ctx->interfaces.cap) {
            ctx->interfaces.cap += 5;
            ctx->interfaces.names = realloc(ctx->interfaces.names, ctx->interfaces.cap * sizeof(vString*));
            if (ctx->interfaces.names == NULL) {
                printf("Failed to reallocate interface names array\n");
                exit(1);
            }
        }
    }
    vString* newIface = vStringNew();
    if (newIface == NULL) {
        printf("Failed to allocate a new vString to hold an interface\n");
        exit(1);
    }
    vStringNCat(newIface, interface, vStringLength(interface));
    ctx->interfaces.names[ctx->interfaces.size++] = newIface;
}

static void parseImplements(parser* ctx)
{
    if (ctx->line == NULL)
        return;
    while (ctx->line != NULL && *ctx->line != 'L')
        ++ctx->line;
    if (ctx->line == NULL)
        return;
    vStringClear(ctx->vs);
    while (ctx->line != NULL && *ctx->line != ';' && *ctx->line != '\0') {
        vStringPut(ctx->vs, (int)*ctx->line);
        ++ctx->line;
    }
    if (ctx->line == NULL)
        return;
    vStringTerminate(ctx->vs);
    addInterface(ctx, ctx->vs);
}

static void parseField(parser* ctx)
{
    doParseItem(ctx, ':', K_FIELD);
}

static void parseMethod(parser* ctx)
{
    doParseItem(ctx, ')', K_METHOD);
}

static void cleanup(parser* ctx)
{
    if (ctx == NULL) {
        return;
    }
    if (ctx->interfaces.names != NULL) {
        for (int i = 0; i < ctx->interfaces.size; ++i) {
            vStringDelete(ctx->interfaces.names[i]);
        }
        free(ctx->interfaces.names);
        ctx->interfaces.names = NULL;
        ctx->interfaces.cap = 0;
        ctx->interfaces.size = 0;
    }
    if (ctx->className != NULL) {
        vStringDelete(ctx->className);
        ctx->className = NULL;
    }
    if (ctx->vs != NULL) {
        vStringDelete(ctx->vs);
        ctx->vs = NULL;
    }
}

static void findSmaliTags()
{
    parser ctx = {
        .className = NULL,
        .interfaces = {
            .names = NULL,
            .cap = 0,
            .size = 0,
        },
        .vs = vStringNew(),
        .ovs = vStringNew(),
        .line = NULL,
    };
    const unsigned char* line;
    while ((line = fileReadLine()) != NULL) {
        ctx.line = chompWhitespace(line);
        if (ctx.line == NULL) {
            continue;
        }
        // Comment -- get out of here.
        if (*ctx.line == '#') {
            continue;
        }
        // Everything needs the classname to be findable.
        if (ctx.className == NULL) {
            if (strncmp((const char*)ctx.line, CLASS_DEF, CLASS_DEF_LEN) == 0) {
                ctx.line = chompWhitespace(ctx.line + CLASS_DEF_LEN);
                int ret = parseClass(&ctx);
                // If we failed here or we found an interface let's just leave.
                if (ret == -1 || ret == 1) {
                    goto done;
                }
            }
        } else {
            if (strncmp((const char*)ctx.line, IMPLEMENTS_PREF, IMPLEMENTS_PREF_LEN) == 0) {
                ctx.line = chompWhitespace(ctx.line + IMPLEMENTS_PREF_LEN);
                parseImplements(&ctx);
            } else if (strncmp((const char*)ctx.line, METHOD_DEF, METHOD_DEF_LEN) == 0) {
                ctx.line = chompWhitespace(ctx.line + METHOD_DEF_LEN);
                parseMethod(&ctx);
            } else if (strncmp((const char*)ctx.line, FIELD_DEF, FIELD_DEF_LEN) == 0) {
                ctx.line = chompWhitespace(ctx.line + FIELD_DEF_LEN);
                parseField(&ctx);
            }
        }
    }
done:
    cleanup(&ctx);
}

extern parserDefinition* SmaliParser(void)
{
    char* v = getenv("SMALI_CTAGS_ESCAPE_VIM");
    if (v != NULL) {
        SMALI_CTAGS_ESCAPE_VIM = 1;
    }
    static const char* const extensions[] = { "smali", NULL };
    parserDefinition* def = parserNew("Smali");
    def->kinds = SmaliKinds;
    def->kindCount = KIND_COUNT(SmaliKinds);
    def->extensions = extensions;
    def->parser = findSmaliTags;
    return def;
}
