Smali parser for ctags

# Installation

1. Move the smali.c file to the ctags source directory.
2. Add smali.c to the source.mak file's `SOURCES` and `OBJECTS`.
3. add SmaliParser to `PARSERS_LIST` in parsers.h
4. Build ctags

This parser is pretty simple: it will find classes, fields, and methods, but
it won't know anything about super classes and the like. It also will be able 
to find implementations of interfaces.

Some info about the generated tags:

Fields include the class name and the type so something like:

```
.field private final foo:Ljava/lang/String;
```

will make a tag like:

```
Lfoo/bar/baz/Clazz;->foo:Ljava/lang/String;
```

Methods include the class name as well so something like:

```
.method protected static foo(Ljava/lang/String;)V
```

will make a tag like:

```
Lfoo/bar/baz/Clazz;->foo(Ljava/lang/String;)
```

The consequence of this (at least in vim) is:

1. To use a class tag you simply place your cursor somewhere in the class string
2. To use a field tag you have to specify everything up to and including the ;
3. To use a method tag you have to specify everything including the parameters


Note that `[` can appear in smali method arguments. I'm not an expert on ctags 
or how people use it, so I don't want to escape these by default, but I use 
this in vim and these need to be escaped there. To get around that, you can 
define the environmental variable `SMALI_CTAGS_ESCAPE_VIM` when running ctags.

Tags are forced to be generated with a line number when they contain a `$` 
since ctags won't escape `$`s in a tag. You can just run ctags with `-n` to 
generate all tags with line numbers.

## Vim filename hack

Vim doesn't like filenames that have a `$` in them. When 
`SMALI_CTAGS_ESCAPE_VIM` is set, filenames will be output with each `$` escaped 
as `\$`. If you put this in your vim config it should work:

```.vimscript
function SmaliFileFixTagFileName()
    let fname = expand("<afile>")
    let delbuf = 0
    if (stridx(fname, "$") != -1 )
        if (stridx(fname, "\\$") == -1)
            let fname = escape(fname, "$")
        else
            let delbuf = 1
        endif
    endif
    exe "silent doau BufReadPre " . fname | exe "edit " . fname | exe "silent 
    doau BufReadPost " . fname
    if (delbuf == 1)
        exe "bd " . escape(escape(fname, "\\"), "$")
    endif
endfunction

augroup tagfix
    au!
    au BufReadCmd *.smali call SmaliFileFixTagFileName()
augroup end
```
